package com.example.souls;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


import java.util.Timer;
import java.util.TimerTask;

import static com.example.souls.Game.PlayerHp;
import static com.example.souls.Map.info;
import static com.example.souls.Map.startPositions;

public class GameView extends SurfaceView implements Runnable {

    public static int cellX = 15;  //количество клеток на экране
    public static int cellY = 16;  //количество клеток на экране
    public static float unitX = 0; //количество пикселей на клетку
    public static float unitY = 0; //количество пикселей на клетку
    private boolean firstTime = true; //проверка на первый шаг

    private Player player;


    private Map map;
    private int mapnumber=1;

    //карта 0
    private EnemyArcher enemyArcher;
    private EnemyMage enemyMage;

    //карта 1

    private FireBall fireBall;

    private FireBall [] fireballMas = new FireBall[4];
    private Arrow[] arrowMas = new Arrow[8];
    private Arrow arrow;



    private Thread gameThread = null;
    private Paint paint;
    private Canvas canvas;

    //время для основного цикла
    private Timer timer;
    private TimerTask program;

    private int tick=0;                                 //количество тиков программы
    private boolean player_cooldown_movement = true;    //флажок есть ли кулдаун на перемещение у игрока
    private int player_cooldown_movement_tick;          //на каком тике произлшло движение игрока
    private boolean player_invulnerability=false;
    private int player_invulnerability_cooldown;

    private SurfaceHolder surfaceHolder;

    public GameView(Context context) {
        super(context);

        //инициализируем обьекты для рисования
        surfaceHolder = getHolder();
        paint = new Paint();

        // инициализируем поток
        gameThread = new Thread(this);
        gameThread.start();

    }

    @Override
    public void run() {
         timer = new Timer();
         program = new TimerTask() {
            @Override
            public void run() {
                if (!firstTime) {
                    //перемещение игрока
                    if (player_cooldown_movement) {
                        playerUpdatePosition();
                        player_cooldown_movement = false;
                        player_cooldown_movement_tick = tick;
                    }

                    if(tick>player_invulnerability_cooldown+20){
                        player_invulnerability=true;
                    }


                /*if (!firstTime) {
                    if (tick % enemyArcher.speed == 0) {
                        enemyArcherUpdatePosition();
                    }
                }*/
                /*if (!firstTime) {
                    if (tick % enemyMage.speed == 0) {
                        enemyMageUpdatePosition();
                    }
                }*/
                    //расчет кулдауна на перемещение
                    if (tick > player_cooldown_movement_tick + player.speed) {
                        player_cooldown_movement = true;
                    }

                    //перерасчет позиций фаерболов
                    for (int i = 0; i < fireballMas.length; i++) {
                        if (fireballMas[i] != null) {
                            if (tick % fireballMas[i].speed == 0) {
                                fireBallsUpdatePosition(i);
                            }
                        }
                    }
                    //создание новых фаерболов
                    for (int i = 0; i < fireballMas.length; i++) {
                        if(fireballMas[i]==null){
                            createFireball(i);
                        }
                    }

                    //перерасчет позиций стрел
                    for (int i = 0; i < arrowMas.length; i++) {
                        if (arrowMas[i] != null) {
                            if (tick % arrowMas[i].speed == 0) {
                                arrowsUpdatePosition(i);
                            }
                        }
                    }
                    //создание новых стрел
                    for (int i = 0; i < arrowMas.length; i++) {
                        if(arrowMas[i]==null){
                            createArrow(i+4,i);
                        }
                    }
                    for (int i = 0; i < fireballMas.length; i++) {
                        if (fireballMas[i] != null) {
                            if(player.x==fireballMas[i].x&&player.y==fireballMas[i].y && player_invulnerability){
                                player.hitpoints-=50;
                                player_invulnerability=false;
                                player_invulnerability_cooldown=tick;
                            }
                        }
                    }

                    for (int i = 0; i < arrowMas.length; i++) {
                        if (arrowMas[i] != null) {
                            if(player.x==arrowMas[i].x&&player.y==arrowMas[i].y && player_invulnerability){
                                player.hitpoints-=40;
                                player_invulnerability=false;
                                player_invulnerability_cooldown=tick;
                            }
                        }
                    }

                }
                drow();
                tick++;


            }
        };
      timer.schedule(program,0,10);
    }

    private void playerUpdatePosition(){
        player.update();
    }

    private void enemyArcherUpdatePosition(){
        enemyArcher.update();
    }

    private void enemyMageUpdatePosition(){
        enemyMage.update();
    }

   private void arrowsUpdatePosition(int num){
        if(arrowMas[num].update()){
            arrowMas[num]=null;
        }
    }

    private void fireBallsUpdatePosition(int num){
        if(fireballMas[num].update()){
            fireballMas[num]=null;
        }
    }


    private void drow(){
        if (surfaceHolder.getSurface().isValid()) {
            if(firstTime){
                firstTime = false;
                unitX = surfaceHolder.getSurfaceFrame().width()/cellX; // вычисляем число пикселей в юните
                unitY = surfaceHolder.getSurfaceFrame().height()/cellY; // вычисляем число пикселей в юните
                player = new Player(getContext());
                map = new Map(getContext(),mapnumber);
                createElems(mapnumber);


            }
            canvas = surfaceHolder.lockCanvas(); // закрываем canvas
            map.drow(paint,canvas); //рисуем карту

            player.drow(paint, canvas);     //рисуем игрока
            PlayerHp.setText(Player.hitpoints + " HP");
            //enemyArcher.drow(paint,canvas);   //рисуем врага
            //enemyMage.drow(paint,canvas);   //рисуем врага
            for(int i=0;i<fireballMas.length;i++) {
                if (fireballMas[i] != null)
                    fireballMas[i].drow(paint, canvas);
            }
            for(int i=0;i<arrowMas.length;i++) {
                if (arrowMas[i] != null)
                    arrowMas[i].drow(paint, canvas);
            }
            surfaceHolder.unlockCanvasAndPost(canvas); // открываем canvas
        }
        }



    private void createElems(int number){
        switch (number){
            case 0:
                player.x=3;
                player.y=3;
                enemyArcher = new EnemyArcher(getContext());
                enemyMage = new EnemyMage(getContext());
                enemyMage.x=7;
                enemyMage.y=11;
                break;
            case 1:
                player.x=2;
                player.y=11;
                //создание фаерболов
                for(int i=0;i<info[0];i++){
                    createFireball(i);
                }
                //создание стрел
                for(int i=info[0];i<info[0]+info[1];i++) {
                    createArrow(i,i-info[0]);
                }
                break;
        }
    }

    private void createFireball(int number){
        fireBall = new FireBall(getContext());
        fireBall.x=startPositions[0][number];
        fireBall.y=startPositions[1][number];
        fireBall.direction=startPositions[2][number];
        switch (fireBall.direction){
            case 1:
                fireBall.bitmapId=R.drawable.fireballleft;
                fireBall.init(getContext());
                break;
            case 2:
                fireBall.bitmapId=R.drawable.fireballdown;
                fireBall.init(getContext());
                break;
            case 3:
                fireBall.bitmapId=R.drawable.fireballright;
                fireBall.init(getContext());
                break;
        }

        fireballMas[number]=fireBall;
    }

    private void createArrow(int number, int pos){

        arrow = new Arrow(getContext());
        arrow.x=startPositions[0][number];
        arrow.y=startPositions[1][number];
        arrow.direction=startPositions[2][number];
        switch (arrow.direction){
            case 1:
                arrow.bitmapId=R.drawable.arrowleft;
                arrow.init(getContext());
                break;
            case 2:
                arrow.bitmapId=R.drawable.arrowdown;
                arrow.init(getContext());
                break;
            case 3:
                arrow.bitmapId=R.drawable.arrowright;
                arrow.init(getContext());
                break;
        }
        arrowMas[pos]=arrow;
    }


}
