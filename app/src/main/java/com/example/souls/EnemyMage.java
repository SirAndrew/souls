package com.example.souls;

import android.content.Context;

public class EnemyMage extends Unit {
    public EnemyMage(Context context) {
        bitmapId = R.drawable.mage;
        x=10;
        y=10;
        speed=10;
        direction=-1;
        init(context);

    }
    @Override
    public void update(){

        x+=direction;
        if(x<1){
            direction=1;
        }
        if(x>14){
            direction=-1;
        }
    }
}
