package com.example.souls;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Game extends AppCompatActivity implements View.OnTouchListener {

    public static boolean isLeftPressed = false;
    public static boolean isRightPressed = false;
    public static boolean isTopPressed = false;
    public static boolean isBotPressed = false;
    public static Button MoveLeftButton;
    public static Button MoveRightButton;
    public static Button MoveTopButton;
    public static Button MoveBotButton;
    public static TextView PlayerHp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.game_layout);
        GameView gameview = new GameView(this);
        LinearLayout gameLayout = (LinearLayout) findViewById(R.id.gameLayout);
        gameLayout.addView(gameview);

        PlayerHp = (TextView) findViewById(R.id.playerhpbar);

        MoveLeftButton = (Button) findViewById(R.id.moveLeftButton);
        MoveRightButton = (Button) findViewById(R.id.moveRightButton);
        MoveTopButton = (Button) findViewById(R.id.moveTopButton);
        MoveBotButton = (Button) findViewById(R.id.moveBotButton);

        MoveLeftButton.setOnTouchListener(this);
        MoveRightButton.setOnTouchListener(this);
        MoveTopButton.setOnTouchListener(this);
        MoveBotButton.setOnTouchListener(this);

    }


    @Override
    public boolean onTouch(View btn, MotionEvent event) {
        switch(btn.getId()) { // определяем какая кнопка
            case R.id.moveLeftButton:
                switch (event.getAction()) { // определяем нажата или отпущена
                    case MotionEvent.ACTION_DOWN:
                        isLeftPressed = true;
                        break;
                    case MotionEvent.ACTION_UP:
                        isLeftPressed = false;
                        break;
                }
                break;
            case R.id.moveRightButton:
                switch (event.getAction()) { // определяем нажата или отпущена
                    case MotionEvent.ACTION_DOWN:
                        isRightPressed = true;
                        break;
                    case MotionEvent.ACTION_UP:
                        isRightPressed = false;
                        break;
                }
                break;

            case R.id.moveTopButton:
                switch (event.getAction()) { // определяем нажата или отпущена
                    case MotionEvent.ACTION_DOWN:
                        isTopPressed = true;
                        break;
                    case MotionEvent.ACTION_UP:
                        isTopPressed = false;
                        break;
                }
                break;

            case R.id.moveBotButton:
                switch (event.getAction()) { // определяем нажата или отпущена
                    case MotionEvent.ACTION_DOWN:
                        isBotPressed = true;
                        break;
                    case MotionEvent.ACTION_UP:
                        isBotPressed = false;
                        break;
                }
                break;
        }
        return true;
    }
}
