package com.example.souls;

import android.content.Context;

import java.util.Timer;
import java.util.TimerTask;

public class Player extends Unit {

    public Player (Context context){
        bitmapId = R.drawable.playermodel;
        x=3;
        y=3;
        speed=7;
        hitpoints = 100;
        init(context);
    }

    @Override
    public void update(){

        if(Game.isLeftPressed && x > 0 && Map.map[y][x-1]!=1){
            x --;
        }
        if(Game.isRightPressed && x < GameView.cellX-1&&Map.map[y][x+1]!=1){
            x ++;
        }

        if(Game.isTopPressed && y > 0 && Map.map[y-1][x]!=1){
            y --;
        }

        if(Game.isBotPressed && y < GameView.cellY-1 && Map.map[y+1][x]!=1){
            y ++;
        }

    }
}
