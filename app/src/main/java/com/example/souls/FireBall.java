package com.example.souls;

import android.content.Context;

public class FireBall extends Projectile {
    public FireBall (Context context){
        bitmapId = R.drawable.fireballup;
        x=12;
        y=11;
        direction=0; //0 вверх, 1 влево, 2 вниз, 3 вправо
        speed=15;
        init(context);
    }
    @Override
    public boolean update(){
        switch (direction){
            case 0: if(Map.map[y-1][x]!=1){
                y--;
            }
            else{
                return true;
            }
                break;
            case 1: if(Map.map[y][x-1]!=1){
                x--;
            }
            else{
                return true;
            }
            break;
            case 2: if(Map.map[y+1][x]!=1){
                y++;
            }
            else{
                return true;
            }
            break;
            case 3: if(Map.map[y][x+1]!=1){
                x++;
            }
            else{
                return true;
            }
            break;
        }
        return false;
    }
}
