package com.example.souls;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

public class Map {
    protected  int bitmapId; // id картинки
    protected Bitmap bitmapMap; // картинка
    public static int [] [] map =  {};
    public static int [] [] startPositions ={};
    public static int [] info ={};
    private int [][] level1 ={
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {1,0,0,0,0,1,1,1,1,1,0,0,0,0,1},
            {1,0,0,0,0,1,0,0,0,1,0,0,0,0,1},
            {1,0,0,0,0,1,0,0,0,1,0,0,0,0,1},
            {1,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
            {1,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
            {1,0,0,0,0,1,0,0,0,1,0,0,0,0,1},
            {1,0,0,0,0,1,1,1,1,1,0,0,0,0,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
    };
    private int [][] level2 = {
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,0,1,1,1,0,1,0,0,1,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,1,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,1,1},
            {1,1,0,0,1,1,1,1,1,1,0,0,0,1,1},
            {1,1,0,0,1,0,0,0,0,1,1,0,0,1,1},
            {1,0,0,0,1,0,0,0,0,1,1,0,0,1,1},
            {1,1,0,0,1,0,0,0,0,1,1,0,0,0,1},
            {1,1,0,0,0,0,0,0,0,0,1,0,0,1,1},
            {1,1,0,0,0,0,0,0,0,1,1,0,0,1,1},
            {1,1,1,1,0,0,0,0,0,1,0,0,0,1,1},
            {1,1,0,1,1,1,1,1,1,0,0,0,0,1,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {1,1,1,1,1,1,0,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}


    };

    private int [][][] mapbook = {level1,level2};

    private int [][] startPos1 = {

    };

    private int [][] startPos2 = {
            {13,2,12,1,13,11,1,3,5, 6, 7, 8}, //координата X
            {13,9,1 ,2,12,1 ,3,9,10,10,10,10},//координата Y
            {1, 0,2, 3,1, 2, 3,0,0, 0, 0, 0}  //направление
    };

    private int [][][] startPositionsBook = {startPos1, startPos2};

    //количество фаерболов, стрел, лучников, магов
    private int [] infomap1 = {};
    private int [] infomap2 = {4,8,0,0};

    private int [][] infomapbook = {infomap1,infomap2};

    public Map (Context context,int number){
        map = mapbook[number];
        startPositions = startPositionsBook[number];
        info = infomapbook[number];
        switch (number){
            case 0: bitmapId = R.drawable.field;
                break;
            case 1: bitmapId = R.drawable.trapmap;
                break;
        }
        init(context);
    }

    void init(Context context) { // сжимаем картинку до нужных размеров
        Bitmap cBitmap = BitmapFactory.decodeResource(context.getResources(), bitmapId);
        bitmapMap = Bitmap.createScaledBitmap(
                cBitmap, (int)(GameView.unitX*GameView.cellX), (int)(GameView.unitY*GameView.cellY), false);
    }

    void drow(Paint paint, Canvas canvas){ // рисуем картинку
        canvas.drawBitmap(bitmapMap, 0, 0, paint);
    }


}
