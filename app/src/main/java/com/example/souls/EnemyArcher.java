package com.example.souls;

import android.content.Context;

public class EnemyArcher extends  Unit {
    public EnemyArcher(Context context){
        bitmapId = R.drawable.archer;
        x=10;
        y=10;
        direction=-1;
        speed=5;
        init(context);
    }
    @Override
    public void update(){

        x+=direction;
        if(x<1){
            direction=1;
        }
        if(x>14){
            direction=-1;
        }
    }
}
