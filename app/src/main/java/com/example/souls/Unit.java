package com.example.souls;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

public class Unit {
    public int x; // координаты
    public int y;
    public int direction; //временно
    public  int speed;
    protected int bitmapId; // id картинки
    protected Bitmap bitmap; // картинка
    public static int hitpoints;

    void init(Context context) { // сжимаем картинку до нужных размеров
        Bitmap cBitmap = BitmapFactory.decodeResource(context.getResources(), bitmapId);
        bitmap = Bitmap.createScaledBitmap(
                cBitmap, (int)(GameView.unitX), (int)(GameView.unitY), false);
        cBitmap.recycle();
    }

    void update(){ // тут будут вычисляться новые координаты
    }

    void drow(Paint paint, Canvas canvas){ // рисуем картинку
        canvas.drawBitmap(bitmap, x*GameView.unitX, y*GameView.unitY, paint);
    }
}
